FROM node:14.21.3-alpine3.17

WORKDIR /app

COPY . .

# RUN npm i sequelize-cli -g # backend
RUN npm install pm2 -g
RUN npm install

EXPOSE 5000

CMD [ "pm2-runtime", "ecosystem.config.js" ]
